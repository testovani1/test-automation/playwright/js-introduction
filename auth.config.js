// global-setup.js
const { chromium } = require('@playwright/test');

module.exports = async config => {
  const browser = await chromium.launch();
  const page = await browser.newPage();
  await page.goto('https://netflix.com/');
  await page.click('text=Sign In');
  await page.fill('input[name="userLoginId"]', 'user');
  await page.fill('input[name="password"]', 'password');
  await page.click('.login-button');
  await page.context().storageState({ path: 'netflix.json' });
  await browser.close();
};