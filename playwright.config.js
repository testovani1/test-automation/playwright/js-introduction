/**@type {import ("@playwright/test").PlaywrightTestConfig} */

const config = {
  use: {
    baseURL: "http://www.uitestingplayground.com",
  },
  globalSetup: require.resolve("./auth.config")
};

module.exports = config;