import {expect, test} from "@playwright/test";

test("fill a form", async () => {
  const prohlizec = await playwright["chromium"].launch({
    headless: false,
    slowMo: 10
  })

const kontext = await prohlizec.newContext();
const stranka = await kontext.newPage();
await stranka.setViewportSize({
  width: 1440,
  height: 900
});

await stranka.goto("http://executeautomation.com/demosite/Login.html");
await stranka.type("[name=UserName]", "executeautomation");
await stranka.type("[name=Password]", "admin");
await stranka.keyboard.press("Enter", {delay: 2000});

await stranka.waitForSelector("input[id=Initial]");

await stranka.hover("[id='Automation Tools']");
await prohlizec.close();
});


test("test request", async () => {
  
  for (const typProhlizece of ["chromium"]) {
        const prohlizec = await playwright[typProhlizece].launch({
          headless: false,
          devtools: true,
      });
    
      const kontext = await prohlizec.newContext();
      const stranka = await kontext.newPage();
  
      let odpocitavadlo = 0;
      await stranka.goto("https://www.seznam.cz/cz/");
      await stranka.waitForResponse(odpoved => {
        console.debug("Čeká se... " + odpocitavadlo);
        odpocitavadlo++;
        return odpoved.request().resourceType() === "xhr";
    })
  
      await stranka.screenshot({path: `./snimky/${typProhlizece}_snimek-${Date.now()}.png`});
          
      await prohlizec.close();
    }
  });

  
test("test login", async () => {
  const prohlizec = await playwright["chromium"].launch({
    headless: false
  });

  const kontext = await prohlizec.newContext();

  const stranka = await kontext.newPage();

  await stranka.goto("http://executeautomation.com/demosite/Login.html");

  await stranka.screenshot({path: `./snimky/ut-${Date.now()}.png`});

  await prohlizec.close();
});


test("principal-snimky", async () => {
  
  const webovky = [
    {web: "https://www.guru99.com/", adresa: "https://www.guru99.com/web-application-testing.html"},
    {web: "https://kitner.cz/", adresa: "https://kitner.cz/testovani_softwaru/prehled-konferenci-o-testovani-software-pro-rok-2022/"}
  ]
  
    for (const typProhlizece of ["chromium"]) {
      const prohlizec = await playwright[typProhlizece].launch({
        headless: false
      });
    
    const kontext = await prohlizec.newContext();
    const stranka = await kontext.newPage();
  
    for (const webovka of webovky) {
      await stranka.goto(webovka.adresa);
      await stranka.screenshot({path: `./snimky/${typProhlizece}_snimek-${Date.now()}_${webovka.web}.png`});
    }      
      await prohlizec.close();
    }
  });

  test("basic test", async ({ page }) => {
    await page.goto("https://playwright.dev");
    const nazev = await page.innerText(".navbar__title");
    expect(nazev).toBe("Playwright");
  });

test("test hidding scrollbar", async ({ page, context }) => {
  // tracing is useful when the test needs to be debugged
  
  // await context.tracing.start({ screenshots: true, snapshots: true })
  await page.goto("/scrollbars");
  await page.click("#hidingButton");
  // await context.tracing.stop({ path: "trace.zip" });
})

test.use({ storageState: "netflix.json" })
test("auth netflix", async ({ page }) => {
  await page.goto("https://www.netflix.com/browse");
  const text = await page.innerText(".profile-gate-label");
  expect(text).toBe("Who's watching?");
})

test.describe("Dynamic scenario", () => {
  test("Goes to page", async ({ page }) => {
    await page.goto("/dynamicid");
  });

  test("Does the Same", async ({ page }) => {
    await page.goto("/dynamicid");
  })
})